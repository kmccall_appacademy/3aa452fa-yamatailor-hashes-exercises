# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str.split(' ').reduce({}) do |obj, word|
    obj[word] = word.length
    obj
  end
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.index(hash.values.max)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  obj = {}
  word.chars.each do |ch|
    obj.has_key?(ch) ? obj[ch] += 1 : obj[ch] = 1
  end
  obj
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  obj = {}
  arr.each { |el| obj[el] = true }
  obj.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  obj = { odd: 0, even: 0 }
  numbers.each { |n| n % 2 == 0 ? obj[:even] += 1 : obj[:odd] += 1 }
  obj
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  count = 0
  letter = []
  string.each_char do |ch|
    if string.count(ch) >= count
      count = string.count(ch)
      letter << ch
    end
  end
  letter.sort.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  list = students.select { |k,v| v >= 7 }.keys
  list.inject([]) do |arr, student|
    n = list.index(student)
    while n + 1 < list.length
      arr << [student, list[n + 1]]
      n += 1
    end
    arr
  end
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  smallest_population = 1
  largest_population = 1
  number_of_species = 1
  obj = {}
  specimens.each do |specimen|
    obj.has_key?(specimen) ? obj[specimen] += 1 : obj[specimen] = 1
    smallest_population = obj.values.min
    largest_population = obj.values.max
    number_of_species = obj.keys.length
  end
  number_of_species**2 * smallest_population / largest_population
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign = normal_sign.delete("!,;'.")
  vandalized_sign = vandalized_sign.delete("!,;'.")

  normal_sign_count = Hash.new(0)
  vandalized_sign_count = Hash.new(0)

  normal_sign.each_char { |ch| normal_sign_count[ch.downcase] += 1 }
  vandalized_sign.each_char { |ch| vandalized_sign_count[ch.downcase] += 1 }

  vandalized_sign_count.each do |letter, count|
    return false if normal_sign_count[letter] < count
  end

  true
end

def character_count(str)
  ch_count = {}
  str.each_char do |ch|
    next if ch = ' '
    ch_count[ch] += 1
  end

  ch_count
end
